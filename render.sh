set -o noclobber
if [ -f "$1/index.js" ]; then
  node render.js "$1" "$2"
fi
