module.exports = {
  pageTitle: "Archive",
  extends: __dirname + '/../../../_templates/base',
  url: "/blog",
  list: false,
  partials: {
    body: __dirname + "/body.md"
  }
}
