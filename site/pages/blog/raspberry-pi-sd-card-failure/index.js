module.exports = {
  pageTitle: "The death of an SD Card",
  url: "/blog/raspberry-pi-sd-card-failure",
  extends: __dirname + '/../../../_templates/article',
  date: '2019-03-04',
  list: true,
  partials: {
    article: __dirname + '/post.md'
  }
}
